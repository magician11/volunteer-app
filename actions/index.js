/*
Process the incoming interactions (button clicks) from the Slack channel.
*/

module.exports = app => {
  const { postToSlack, postToWebAPI } = require('../modules/slack');

  app.post('/interact', async (req, res) => {
    const slackMessage = JSON.parse(req.body.payload);

    res.sendStatus(200);
    const { original_message, actions, callback_id, channel } = slackMessage;
    const personWhoClickedButton = slackMessage.user.name;

    switch (callback_id) {
      case 'volunteer_tasks': {
        // first find the task that matches the button clicked
        for (
          let volunteerTask = 0;
          volunteerTask < original_message.attachments.length;
          volunteerTask += 1
        ) {
          // once we found the task someone volunteered for
          if (
            original_message.attachments[volunteerTask].fields[0].value ===
            actions[0].name
          ) {
            // decrement the available positions left
            original_message.attachments[volunteerTask].fields[1].value -= 1;

            // add that user to the list of volunteers
            const peopleWhoVolunteered =
              original_message.attachments[volunteerTask].fields[2].value;
            original_message.attachments[volunteerTask].fields[2].value =
              peopleWhoVolunteered === 'none yet'
                ? personWhoClickedButton
                : `${peopleWhoVolunteered}, ${personWhoClickedButton}`;

            // if there are no more available positions for this task, then remove the button
            if (
              original_message.attachments[volunteerTask].fields[1].value === 0
            ) {
              original_message.attachments[volunteerTask].actions = [];
            }
          }
        }

        postToSlack(original_message, slackMessage.response_url);

        postToWebAPI(
          {
            token: process.env.VOLUNTEER_SLACK_APP_TOKEN,
            channel: channel.id,
            text: `${personWhoClickedButton} just volunteered for ${actions[0]
              .name}`
          },
          'https://slack.com/api/chat.postMessage'
        );

        break;
      }
      default: {
        postToSlack(
          { text: 'This interaction is not known.' },
          slackMessage.response_url
        );
      }
    }
  });
};
