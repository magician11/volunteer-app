const request = require('request');
const rpn = require('request-promise-native');

const postToWebAPI = (formData, uri) => {
  const options = {
    uri,
    form: formData
  };

  return rpn.post(options);
};

const postToSlack = (slackData, uri) => {
  const options = {
    uri,
    json: slackData
  };

  return rpn.post(options);
};

module.exports = {
  postToSlack,
  postToWebAPI
};
