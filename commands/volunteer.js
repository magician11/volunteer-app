/*
Volunteer command for the Volunteer app

Intended use: /volunteer [task] ([number of people] [optionally - random] [optionally -all .. assumes only logged in users]), [add tasks..]

e.g.
/volunteer task1 (1), another long task (3), task3 is here (5)
*/

module.exports = app => {
  const sampleSize = require('lodash.samplesize');
  const { postToSlack, postToWebAPI } = require('../modules/slack');

  app.post('/volunteer', async (req, res) => {
    const {
      text,
      token,
      channel_name,
      user_name,
      response_url,
      channel_id,
      team_domain
    } = req.body;
    const tasks = text.split(',');

    // initial feedback
    res.json({
      text: `Setting up the volunteer info for you now ${user_name}. One moment please...`
    });

    const taskMessage = {
      attachments: []
    };
    // go through the tasks and construct response for Slack
    for (let task of tasks) {
      const taskDetails = task.match(/(.+)\((.+)\)/);

      if (taskDetails === null) {
        taskMessage.attachments.push({
          text:
            'Whoops.. you need to specify the command like `/volunteer here is a task (3)`',
          mrkdwn_in: ['text']
        });
        postToSlack(taskMessage, response_url);
        return;
      }

      const taskDescription = taskDetails[1].trim();
      const taskOptions = taskDetails[2].trim();

      // if the option for this task is only numbers, then show volunteer buttons
      if (taskOptions.match(/^[0-9]+$/)) {
        taskMessage.response_type = 'in_channel';
        const numberOfVolunteers = parseInt(taskOptions, 10);

        taskMessage.attachments.push({
          text: 'We are looking for volunteers!',
          fields: [
            {
              title: 'Task',
              value: taskDescription,
              short: true
            },
            {
              title: 'Positions available',
              value: numberOfVolunteers,
              short: true
            },
            {
              title: 'Volunteers',
              value: 'none yet'
            }
          ],
          callback_id: 'volunteer_tasks',
          actions: [
            {
              name: taskDescription,
              text: 'Volunteer',
              type: 'button',
              value: 'volunteer'
            }
          ]
        });
      } else {
        // otherwise assign out random people to this task
        const params = taskOptions.split(' ');
        const volunteersToAssign = parseInt(params.shift());

        try {
          const channelData = await postToWebAPI(
            {
              token: process.env.VOLUNTEER_SLACK_APP_TOKEN,
              channel: channel_id
            },
            'https://slack.com/api/channels.info'
          );

          let randomlyAssignedVolunteers = [];
          if (params.includes('-r')) {
            let members = JSON.parse(channelData).channel.members;

            if (!params.includes('-a')) {
              const onlineMembers = [];
              for (let memberId of members) {
                const status = await postToWebAPI(
                  {
                    token: process.env.VOLUNTEER_SLACK_APP_TOKEN,
                    user: memberId
                  },
                  'https://slack.com/api/users.getPresence'
                );
                const presence = JSON.parse(status).presence;
                if (presence === 'active') {
                  onlineMembers.push(memberId);
                }
              }
              members = onlineMembers;
            }

            randomlyAssignedVolunteers = sampleSize(
              members,
              volunteersToAssign
            );

            taskMessage.response_type = 'in_channel';
            taskMessage.attachments.push({
              text: 'Volunteer positions assigned out.',
              fields: [
                {
                  title: 'Task',
                  value: taskDescription
                },
                {
                  title: `Volunteers (${volunteersToAssign})`,
                  value: `${randomlyAssignedVolunteers.map(
                    member => `<@${member}>`
                  )}`
                }
              ],
              callback_id: 'volunteer_tasks'
            });
          } else {
            taskMessage.text =
              'Did you forget to specify the `-r` flag for the number of volunteers?';
          }
        } catch (error) {
          console.log(error);
        }
      }
    }
    postToSlack(taskMessage, response_url);
  });
};
