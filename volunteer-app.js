const express = require('express');
const https = require('https');
const fs = require('fs');
const bodyParser = require('body-parser');

const app = express();
app.use(bodyParser.json());
app.use(bodyParser.urlencoded({ extended: true }));

// import the routes
require('./commands/volunteer')(app);
require('./actions')(app);

const PORT = 5798;

// Make sure all connections are encrypted
const sslOptions = {
  key: fs.readFileSync('/etc/letsencrypt/live/volunteerengine.com/privkey.pem'),
  cert: fs.readFileSync('/etc/letsencrypt/live/volunteerengine.com/fullchain.pem'),
  ca: fs.readFileSync('/etc/letsencrypt/live/volunteerengine.com/chain.pem')
};

// startup the app
https.createServer(sslOptions, app).listen(PORT, () => {
  console.log(
    `Volunteer App started listening on port ${PORT} at ${new Date().toString()}.`
  );
});
